class Student:
    # data member
    # name = ''
    # email = ""

    def storeData(q,name,email):
        q.n  = name
        q.e = email
        print('Name and Email stored.')

    # member fn
    def showData(f):
        print(f.n)
        print(f.e)
        print('function in class')

d = Student()
d.storeData('pluto','p@gmail.com')
d.showData()
d.storeData('pluto12','p@gmail.com12')
d.showData()
