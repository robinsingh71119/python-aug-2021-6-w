import random
class Student:
    def regStu(r,name,email):
        r.name = name
        r.email = email
        r.roll_no = random.randint(1,99)
        print('Student Register Successfully')
    
    def showDetails(r):
        print(f"Your name {r.name}, Roll no {r.roll_no}")

# d = Student()
# d.regStu('')
# d.showDetails()

class Account(Student):
    def createAcc(d):
        d.acc_no = random.randint(10000,99999)
        print(d.acc_no)
    def showAccDet(d):
        print(f" {d.name} . {d.acc_no}")

accObj = Account()
accObj.regStu('jarvis','j@gmal.com')
accObj.showDetails()
accObj.createAcc()
accObj.showAccDet()
