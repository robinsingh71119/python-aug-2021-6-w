rec1 = {1,5,12,44,7,2,3,4,5,6,7,8}
rec2 = {'j','f','m','a','m','j','j'}
rec3 = {1,2,3,4,5,6,7,8}
print(rec1)
print(rec2)
# is mutable
# rec1.add(1212)
rec3.add(878787)
rec1.pop()
print(rec1)
print(rec3)