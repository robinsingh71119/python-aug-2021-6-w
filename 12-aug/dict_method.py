studentRecord = {
        'name':'Jarvis',
        'age':25,
        'hobbies':['playing','movie','reading'],
        'standard':'9',
        'subject':('Hindi','english','match','science'),
        'technology':
            {
            'front':['html','css','js'],
            'backend':['python','django'],
            },
}

# get
# print(studentRecord['technology']['backend'][1])
# print(studentRecord.get('standard'))

# add / update
studentRecord['hobbies'] = 'new hobbie' # update
studentRecord['dob'] = '12-12-1994' # add
# print(studentRecord)


# dynamic dict
newRecord = {}
newRecord['name'] = 'Jarvis'
newRecord['age'] = 55
# print(newRecord)

# for i in studentRecord:
#     print(i,':',studentRecord[i] )

# values , items , key

# print(studentRecord.values())
# print(studentRecord.items())
# print(studentRecord.keys())
# print(len(studentRecord))

# remove / pop
# studentRecord.popitem()
# print('pop return =>',studentRecord.pop('name'))
print(' ')
# del studentRecord['hobbies']

# studentRecord.clear()
print(studentRecord)
print(' ')
print(' ')
del studentRecord
studentRecord['dd'] = 'dfdf'
print(' ')
print(' ')
print(studentRecord)
for i in studentRecord:
    print(i,':',studentRecord[i] )
