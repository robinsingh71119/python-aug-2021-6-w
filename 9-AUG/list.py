colors = ['red','green','blue','orange','pink','gray']
# variable = [0,1,2,3,4,5,6,7...]
print(colors)

# Indexing
print(colors[2])
print(colors[-3])

# slicing
print(colors[1:-1])

# step slicing
print(colors[1:-1:2])