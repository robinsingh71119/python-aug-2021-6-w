colors = ['red','green','blue','orange','pink','gray']
shades = ['dark','light','faddy','shiny']
print(colors)

# add
# colors.append('violet')
# colors.insert(4,'cream')
# print(colors)

# update
# colors[4] = 'newcolor'
# print(colors)

# pop / remove
# colors.pop()
# colors.pop(4)
# colors.remove('blue')
print(colors)

print(len(colors))
print(colors.index('blue'))