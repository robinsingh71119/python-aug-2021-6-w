rec1 = {1,5,12,44,7,2,3,4,5,6,7,8}
rec2 = {1,2,3,4,5,6,7,8,1399}

# union
# print(rec1.union(rec2))
# print(rec1 | rec2)

# intersection

# print(rec1.intersection(rec2))
# print(rec2 & rec1)

# difference
# print(rec1.difference(rec2))
# print(rec2.difference(rec1))
# print(rec2 - rec1)

# symmetric difference
print(rec1.symmetric_difference(rec2))
print(rec2.symmetric_difference(rec1))
print(rec2 ^ rec1)