"""mypro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from myapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index,name='index'),
    path('/ab',views.about,name='about_us'),
    path('table',views.table,name='table_page'),
    path('/img',views.image_,name='image_page'),
    path('/div_span',views.div_span,name='div_span'),
    path('/i_frame_bg_img',views.i_frame_bg_img,name='i_frame_bg_img'),
    path('/position_css',views.position_css,name='position_css'),
    path('/position_',views.position_,name='position_'),
    path('/pseudo_element',views.pseudo_element,name='pseudo_element'),
    path('/input_',views.input_,name='input_'),
    path('/more_input',views.more_input,name='more_input'),
    path('/keyframe',views.keyframe,name='keyframe'),
    path('/video_audio',views.video_audio,name='video_audio'),
    path('/add_data',views.add_data,name='add_data'),
]
