from django.shortcuts import render
from django.http import HttpResponse
def index(s):
    return render(s,'index.html')

def about(a):
    return render(a,'internal_css_anchor_tag.html')

def table(a):
    return render(a,'table.html')

def image_(a):
    return render(a,'img.html')

def div_span(a):
    return render(a,'div_span.html')

def i_frame_bg_img(a):
    return render(a,'i_frame_bg_image.html')

def position_css(a):
    return render(a,'position_css.html')

def position_(a):
    return render(a,'position_fixed_sticky.html')

def pseudo_element(a):
    return render(a,'pseudo_element.html')

def input_(a):
    return render(a,'input.html')

def more_input(a):
    return render(a,'more_input.html')

def keyframe(a):
    return render(a,'keyframe.html')

def video_audio(a):
    return render(a,'video_audio.html')

def add_data(a):
    # table 
    return HttpResponse(a)
    