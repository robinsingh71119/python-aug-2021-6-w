from django.contrib import admin
from myapp.models import addRecord
# Register your models here.

class adminAddRecord(admin.ModelAdmin):
    list_display = ['username','email']

admin.site.register(addRecord,adminAddRecord)