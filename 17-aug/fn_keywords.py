def details_(name,email,password):
    print(f"Name {name} , Email {email}, Password  {password}")
    # print("Name {name} , Email {email}, Password  {password}".format())

# positional arguments
details_('jarvis','jarvis@gmail.com','1212')

# keywords arguments
details_(email='jarvis@gmail.com',name='jarvis',password='1212')

# positional plus keyword
details_('jarvis','jj@gmail.com',password='1212')
# keyword plus positional
# details_(password='1212',email='jj@gmail.com','jarvis')

