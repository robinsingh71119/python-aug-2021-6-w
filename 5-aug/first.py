# comment
a = 10 # int
b = "Hello" # string
c = 10.22 # float
print(a)
print(type(a))
print(b)
print(type(b))
print(c)
print(type(c))

# list
tech = ['php','python','javascript'] # mutable
print(tech)
print(type(tech))

# tuple
tech = ('php','python','javascript') # immutable
print(tech)
print(type(tech))

# dictionary
record = {"name":'Jarvis',"age":25,"location":"USA"}
print(record)
print(type(record))

# set
record1 = {1,2,3,4,5}
print(record1)
print(type(record1))

# boolean
todayAttendance = False
print(todayAttendance)