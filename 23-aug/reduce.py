import random
from functools import reduce
cgpa = []

for i in range(7):
    randNo = round(random.uniform(4,9.99),2)
    cgpa.append(randNo)

print(cgpa)

# Perc = list(map(lambda x:x*9.5 , cgpa))
# print(Perc)

marksObt = reduce(lambda x,y:x+y , cgpa)
totalMarks = 100*len(cgpa)
print(marksObt)
print(totalMarks)
print('Grade =', (marksObt / totalMarks)*100) # BODMAS
