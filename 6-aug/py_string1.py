txt = "Hello everyone i am a python Strin"

# Indexing
print(txt[1])
print(txt[-5])

# find a char.
# print(txt.find('python'))
# print(txt.index('python'))
# val_ = txt.find('python')
# print(txt[val_])

# slicing
# print(variable[initialize:final])
# print(txt[22:28])
# print(txt[10:-15])
# print(txt[-10:])

# step slicing
# print(variable[initialize:final:steps])
print(txt[10:25:1])

# membership
print('am' in txt)
print('am' not in txt)