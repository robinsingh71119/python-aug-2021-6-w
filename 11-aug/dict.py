studentRecord = {
        'name':'Jarvis',
        'age':25,
        'hobbies':['playing','movie','reading'],
        'standard':'9',
        'subject':('Hindi','english','match','science'),
        'technology':
            {
            'front':['html','css','js'],
            'backend':['python','django'],
            },
}

print(studentRecord)
print(studentRecord['hobbies'])
print(studentRecord['hobbies'][-1:])
# studentRecord technology backend first value.