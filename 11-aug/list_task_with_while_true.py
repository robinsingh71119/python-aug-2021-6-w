options = '''
            Option 1 : View Colors
            Option 2 : Add Color Name
            Option 3 : Remove Color Name
            Option 4 : To exit
          '''
print(options)
colors = []
while True:
    choice = raw_input('Enter your option. ')
    if choice == '1':
        print(colors)
    elif choice == '2':
        name = raw_input('Enter Color name. ')
        colors.append(name)
        print("{} color added".format(name))
    elif choice == '3':
        name = raw_input('Enter Color name. ')
        colors.remove(name)
        print("{} color deleted".format(name))
    elif choice == "4":
        break
    else:
        print('Invalid Option')